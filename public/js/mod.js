$(document).ready(() => {
    $('.sound > i').click((e) => {
        let audio = $('audio')
        let target = $(e.target)
        
        new Promise(resolve => {
            for(var i=0; i< audio.length; i++) {
                audio[i].pause()
                audio[i].currentTime = 0
            }
            resolve(target)
        }).then(target => {
            target.siblings('audio').get(0).play()
        })
    })
})